<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php
$subject = $search = $replace = $result = "";
if(isset($_POST['submit'])) {
    $subject = $_POST['subject'];
    $search = $_POST['search'];
    $replace = $_POST['replace'];
    $result = '';
    $result = replace($subject, $search, $replace);
}
?>
<form action="<?= $_SERVER['PHP_SELF'] ?>" method="POST">
    <p>
        <label for="">
            Текст:
            <input type="text" name="subject" value="<?= $subject ?>">
        </label>
    </p>
    <p>
        <label for="">
            Знайти:
            <input type="text" name="search" value="<?= $search ?>">
        </label>
    </p>
    <p>
        <label for="">
            Замінити:
            <input type="text" name="replace" value="<?= $replace ?>">
        </label>
    </p>
    <p>
        <label for="">
            Результат:
            <input type="text" name="result" value="<?= $result ?>">
        </label>
    </p>
    <p>
        <input type="submit" name="submit" value="Ok">
    </p>
</form>
</body>
</html>

<?php

function replace($subject, $search, $replace)
{
    return str_replace($search, $replace, $subject);
}
