<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php
$cities = "";
if (isset($_POST['cities'])) {
    $cities = $_POST['cities'];
    print(alphabeticalSort($_POST['cities']));
} ?>
<form action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
    <input type="text" name="cities" value="<?= $cities?>">
</form>


</body>
</html>

<?php
function alphabeticalSort($string)
{
    // СОРТУЄ ТІЛЬКИ ASCII
    $cities = explode(' ', trim($string));
    sort($cities, SORT_LOCALE_STRING | SORT_FLAG_CASE);
    return join(' ', $cities);
}
