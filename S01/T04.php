<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<form action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
    <input type="text" name="date[]" placeholder="День-Місяць-Рік" value="<?php if(isset($_POST['date'][0])) echo $_POST['date'][0]; ?>">
    <input type="text" name="date[]" placeholder="День-Місяць-Рік" value="<?php if(isset($_POST['date'][1])) echo $_POST['date'][1]?>">
    <input type="submit">
</form>
<?php if($_SERVER['REQUEST_METHOD'] == 'POST' ) {
    $dates = $_POST['date'];
    $incorrect_dates = array_filter($dates, function($date) {
        return date("d-m-Y", strtotime($date)) != $date;
    });

    if(count($incorrect_dates)) {
        echo "Помилка: ", implode(',',$incorrect_dates);
    }
    echo 'Різниця: ', diff_date($_POST['date'][0], $_POST['date'][1]);
}
?>
</body>
</html>
<?php

function diff_date($date1,$date2) {
    $date1 = strtotime($date1);

    $date2 = strtotime($date2);
    if($date1 and $date2)
        return round(($date2 - $date1)/ (60 * 60 * 24));
}