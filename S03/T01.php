<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Завдання 3.1</title>
</head>
<body>
<?php if($_SERVER['REQUEST_METHOD'] == 'GET') { ?>
<form action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
    <input type="text" name="user-name" placeholder="Ім’я">
    <input type="text" name="comment" placeholder="Коментар">
    <input type="submit" value="SEND">
</form>
<?php }
else if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $file = fopen('FILE_T01.csv', 'a+');
    fputcsv($file, array($_POST['user-name'], $_POST['comment']));
    fseek($file, 0);
    echo "<table>";
    while(($data = fgetcsv($file)) !== FALSE) {
        echo "<tr><td>$data[1]</td></tr>";
    }
    echo "</table>";
    fclose($file);
} ?>
</body>
</html>
