<?php
    if(isset($_POST['submit'])) {
        $name = $_POST['name'];
        if(!empty($name)) {
            if(is_dir($name)) {
                echo 'Директорія існує';
            }
            else {
                mkdir($name);
                $sub_dirs = array('video', 'music', 'photo');
                foreach ($sub_dirs as $dir) {
                    $path = $name . '/' . $dir;
                    mkdir($path, recursive: true);
                }
            }

        }
    }
?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Завдання 4.1</title>
</head>
<body>
<form action="<?=$_SERVER['PHP_SELF']?>" method="post">
    <p><label>
            Логін:
            <input type="text" name="name">
        </label></p>
    <p><label>
            Пароль:
            <input type="password" name="password">
        </label></p>
    <p>
        <button type="submit" name="submit">Submit</button></p>
</form>
<a href="delete.php">Вилучити папку</a>
</body>
</html>