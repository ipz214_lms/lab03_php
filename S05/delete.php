<?php
function delTree($dir) {
   $files = array_diff(scandir($dir), array('.','..'));
    foreach ($files as $file) {
      (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file");
    }
    return rmdir($dir);
}
if (isset($_POST['submit'])) {
    $name = $_POST['name'];
    if (!empty($name)) {
        if (is_dir($name)) {
            delTree($name);
        }
        else {
            echo 'директорія не існує';
        }
    }

}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Завдання 4.1</title>
</head>
<body>
<form action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
    <p><label>
            Логін:
            <input type="text" name="name">
        </label></p>
    <p><label>
            Пароль:
            <input type="password" name="password">
        </label></p>
    <p>
        <button type="submit" name="submit">Submit</button>
    </p>
</form>
</body>
</html>